package com.spring.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.spring.mvc.dao.IArticleDao;
import com.spring.mvc.dao.IMvStockDao;
import com.spring.mvc.entities.Article;
import com.spring.mvc.entities.MvStock;
import com.spring.mvc.services.IArticleService;
import com.spring.mvc.services.IMvStockService;

@Transactional
public class MvStockServiceImpl implements IMvStockService{
	
	private IMvStockDao dao;
	
	public void setDao(IMvStockDao dao) {
		this.dao = dao;
	}

	@Override
	public MvStock save(MvStock entity) {
		return dao.save(entity);
	}

	@Override
	public MvStock update(MvStock entity) {
		return dao.update(entity);
	}

	@Override
	public List<MvStock> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<MvStock> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public MvStock getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public MvStock findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvStock findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
