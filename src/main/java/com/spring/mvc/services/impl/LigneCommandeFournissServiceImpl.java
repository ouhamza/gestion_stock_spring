package com.spring.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.spring.mvc.dao.IArticleDao;
import com.spring.mvc.dao.ILigneCommandeFournDao;
import com.spring.mvc.entities.Article;
import com.spring.mvc.entities.LigneCommandeFournisseur;
import com.spring.mvc.services.IArticleService;
import com.spring.mvc.services.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournissServiceImpl implements ILigneCommandeFournisseurService{
	
	private ILigneCommandeFournDao dao;
	
	public void setDao(ILigneCommandeFournDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		return dao.save(entity);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
