package com.spring.mvc.services.impl;

import java.io.InputStream;

import com.spring.mvc.dao.IFlickrDao;
import com.spring.mvc.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService{

	private IFlickrDao dao;
	
	public void setDao(IFlickrDao dao) {
		this.dao = dao;
	}
	
	@Override
	public String savePhoto(InputStream strem, String fileName) throws Exception {
		// TODO Auto-generated method stub
		return dao.savePhoto(strem, fileName);
	}

}
