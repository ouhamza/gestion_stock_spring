package com.spring.mvc.dao;

import com.spring.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient>{

}
