package com.spring.mvc.dao;

import com.spring.mvc.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur>{

}
