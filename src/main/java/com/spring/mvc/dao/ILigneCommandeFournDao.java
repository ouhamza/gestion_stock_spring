package com.spring.mvc.dao;

import com.spring.mvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournDao extends IGenericDao<LigneCommandeFournisseur>{

}
