package com.spring.mvc.dao;

import com.spring.mvc.entities.Client;

public interface IClientDao extends IGenericDao<Client>{

}
