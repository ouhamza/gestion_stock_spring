package com.spring.mvc.dao;

import com.spring.mvc.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
