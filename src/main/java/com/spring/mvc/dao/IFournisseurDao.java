package com.spring.mvc.dao;

import com.spring.mvc.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur>{

}
