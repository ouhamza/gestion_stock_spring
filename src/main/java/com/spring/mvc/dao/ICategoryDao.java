package com.spring.mvc.dao;

import com.spring.mvc.entities.Category;

public interface ICategoryDao extends IGenericDao<Category>{

}
