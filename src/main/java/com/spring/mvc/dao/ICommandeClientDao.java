package com.spring.mvc.dao;

import com.spring.mvc.entities.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient>{

}
