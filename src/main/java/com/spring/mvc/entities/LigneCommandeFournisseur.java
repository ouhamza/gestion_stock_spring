package com.spring.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneCdtFourniss;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCommandeFourniss")
	private CommandeFournisseur CommandeFourniss;

	public LigneCommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdLigneCdtFourniss() {
		return idLigneCdtFourniss;
	}

	public void setIdLigneCdtFourniss(Long idLigneCdtFourniss) {
		this.idLigneCdtFourniss = idLigneCdtFourniss;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFourniss() {
		return CommandeFourniss;
	}

	public void setCommandeFourniss(CommandeFournisseur commandeFourniss) {
		CommandeFourniss = commandeFourniss;
	}

	
}
