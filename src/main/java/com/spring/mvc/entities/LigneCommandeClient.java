package com.spring.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idLigneCodeClt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idCommandeClient")
	private CommandeClient CommandeClient;

	public Long getIdLigneCodeClt() {
		return idLigneCodeClt;
	}

	
	public LigneCommandeClient() {
		super();
		// TODO Auto-generated constructor stub
	}


	public void setIdLigneCodeClt(Long idLigneCodeClt) {
		this.idLigneCodeClt = idLigneCodeClt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeClient() {
		return CommandeClient;
	}

	public void setCommandeClient(CommandeClient commandeClient) {
		CommandeClient = commandeClient;
	}
	
	

}
