package com.spring.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandeFournisseur implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCommandeFourniss;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	
	@ManyToOne
	@JoinColumn(name="idFournisseur")
	private Fournisseur fournisseur;
	
	@OneToMany(mappedBy = "CommandeFourniss")
	private List<LigneCommandeFournisseur> ligneCommandesFourniss;

	public CommandeFournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdCommandeFourniss() {
		return idCommandeFourniss;
	}

	public void setIdCommandeFourniss(Long idCommandeFourniss) {
		this.idCommandeFourniss = idCommandeFourniss;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLigneCommandesFourniss() {
		return ligneCommandesFourniss;
	}

	public void setLigneCommandesFourniss(List<LigneCommandeFournisseur> ligneCommandesFourniss) {
		this.ligneCommandesFourniss = ligneCommandesFourniss;
	}	
	

}
